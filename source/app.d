import std.json;
import std.net.curl;
import std.stdio;

void main()
{
	char[] response;

	try {
		response = get("https://shouldideploy.today/api");
	} catch (CurlException e) {
		writefln("Curl exception: %s", e.msg);
		return;	
	}

	JSONValue parsed = parseJSON(response);
	writeln(parsed["message"].str);
}
